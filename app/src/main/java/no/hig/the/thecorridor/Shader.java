package no.hig.the.thecorridor;

import android.content.Context;
import android.opengl.GLES20;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Even on 04.11.2014.
 */
public class Shader {

    Context context;

    int programHandle;

    // Uniform handles
    int lightColorUniformHandler;
    int ambientLighColorUniformHandler;
    int camPosHandle;
    int lightPosHandle;
    int lightDirectionHandle;


    /**
     * read vs and fs from file, load shader and compile
     *
     * @param context - GameActivity context
     * @param vsId - R.raw, id to vertex shader
     * @param fsId - R.raw, id to fragment shader
     */
    public Shader(Context context, int vsId, int fsId) {
        this.context = context;

        // read shadert code from file
        String vertexShaderCode = readTextFile(context, vsId);
        String fragmentShaderCode = readTextFile(context, fsId);

        // Complie shader code
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        // Create program and attach shaders to program
        programHandle = GLES20.glCreateProgram();
        GLES20.glAttachShader(programHandle, vertexShader);
        GLES20.glAttachShader(programHandle, fragmentShader);
        GLES20.glLinkProgram(programHandle);

        GLES20.glDeleteShader(vertexShader);
        GLES20.glDeleteShader(fragmentShader);

        // Get uniform locations
        lightColorUniformHandler = GLES20.glGetUniformLocation(programHandle, "lightColor");
        ambientLighColorUniformHandler = GLES20.glGetUniformLocation(programHandle, "ambientLight0");

        camPosHandle = GLES20.glGetUniformLocation(programHandle, "uCameraPosition");
        lightPosHandle = GLES20.glGetUniformLocation(programHandle, "uLightPosition");
        lightDirectionHandle = GLES20.glGetUniformLocation(programHandle, "uConeDirection");
    }

    /**
     * Complie shader
     *
     * @param type - fragment shader og vertex shader
     * @param shaderCode - shader source code
     * @return a compiled shader
     */
    private int loadShader(int type, String shaderCode){

        // create a vertex shader type
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    /**
     * Read shadercode from file
     *
     * @param context - context
     * @param RID - file id (R.raw)
     * @return - the file as a string
     */
    public static String readTextFile(Context context, int RID) {
        StringBuilder body = new StringBuilder();
        try {
            InputStream inputStream = context.getResources().openRawResource(RID);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferReader = new BufferedReader(inputStreamReader);
            String line;
            while((line = bufferReader.readLine()) != null) {
                body.append(line + "\n");
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return body.toString();
    }

    /**
     * Use this shader program
     */
    public void bind() {
        GLES20.glUseProgram(programHandle);
    }

    /**
     * Update light uniforms
     *
     * @param lightPosition - the lights position in model space
     * @param cameraPosition - camera position in model space
     * @param lightDirection - the lights direction normalized
     */
    public void update(float[] lightPosition, float[] cameraPosition, float[] lightDirection) {
        GLES20.glUniform3fv(camPosHandle, 1, cameraPosition, 0);
        GLES20.glUniform3fv(lightPosHandle, 1, lightPosition, 0);
        GLES20.glUniform3fv(lightDirectionHandle, 1, lightDirection, 0);
    }

    /**
     * Update light color
     * @param color - light color r g b a
     */
    public void updateLightColor(float color[]) {
        GLES20.glUniform4fv(lightColorUniformHandler, 1, color, 0);
    }

    /**
     * Update ambientlight r g b a
     * @param ambientLight - ambientlight r g b a
     */
    public void updateAmbientLight(float ambientLight[]) {
        GLES20.glUniform4fv(ambientLighColorUniformHandler, 1, ambientLight, 0);
    }

    /**
     * getter for programhandler
     * @return - programHandler
     */
    public int getProgramHandle() {
        return programHandle;
    }
}
