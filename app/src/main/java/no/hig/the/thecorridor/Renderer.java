package no.hig.the.thecorridor;

import android.content.Context;
import android.opengl.GLES20;

import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.EyeTransform;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;

import javax.microedition.khronos.egl.EGLConfig;

/**
 * The rendering system, where the game is controlled from
 */
public class Renderer implements CardboardView.StereoRenderer {

    private Game corridorGame;
    private Context context;

    public Renderer(Context context){
        this.context = context;
    }

    @Override
    public void onNewFrame(HeadTransform headTransform) {
        corridorGame.updateGame(headTransform);
    }

    @Override
    public void onDrawEye(EyeTransform eyeTransform) {
        // Prepare for new drawing
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        corridorGame.render(eyeTransform);
    }

    @Override
    public void onFinishFrame(Viewport viewport) {  }

    @Override
    public void onSurfaceChanged(int i, int i2) {  }

    @Override
    public void onSurfaceCreated(EGLConfig eglConfig) {

        // Set background color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // Enable depth test so nothing is drawn on top of each other
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        // Create the game
        corridorGame = new Game(context);
    }

    @Override
    public void onRendererShutdown() {
        corridorGame.freeResources();
        System.gc();
    }


    public Game getCorridorGame() {
        return corridorGame;
    }

}