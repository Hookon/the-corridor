package no.hig.the.thecorridor;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Added modifications based on http://sourceforge.net/projects/objloaderforand/
 */
public class OBJLoader {
    private Vector<Short> faces=new Vector<Short>();
    private Vector<Short> vtPointer=new Vector<Short>();
    private Vector<Short> vnPointer=new Vector<Short>();

    private Vector<Float> v=new Vector<Float>();
    private Vector<Float> vn=new Vector<Float>();
    private Vector<Float> vt=new Vector<Float>();

    Vector<Vector3f> vertexData = new Vector<Vector3f>();
    Vector<Float> texturecoords = new Vector<Float>();
    Vector<Vector3f> normals = new Vector<Vector3f>();

    public OBJLoader() {

    }

    /**
     * Parse an objFile and init vertex, normal and texture data
     *
     * @param fileName - obj file name
     * @param context - context
     */
    public void parseOBJ(String fileName, Context context) {
        String line;

        BufferedReader reader = null;
        InputStream inputStream;
        String file = "models/" + fileName;
        AssetManager assetManager = context.getAssets();

        //try to open file
        try {
            inputStream = assetManager.open(file);
            reader = new BufferedReader(new InputStreamReader(inputStream));
        } 		catch(IOException e){
        }
        //try to read lines of the file
        try {
            // Parse file
            while((line = reader.readLine()) != null) {
                if(line.startsWith("f")){//a polygonal face
                    processFLine(line);
                }
                else
                if(line.startsWith("vn")){
                    processVNLine(line);
                }
                else
                if(line.startsWith("vt")){
                    processVTLine(line);
                }
                else
                if(line.startsWith("v")){ //line having geometric position of single vertex
                    processVLine(line);
                }
            }
            // Create vertices, texturecoords and normals for glDrawArrays
            initVertices();
            initTextureCoords();
            initNormals();
        }
        catch(IOException e){
            System.out.println("wtf...");
        }
    }

    /**
     * parse vertex line
     * @param line - one line from file
     */
    private void processVLine(String line){
        String [] tokens=line.split("[ ]+"); //split the line at the spaces
        int c=tokens.length;
        for(int i=1; i<c; i++){ //add the vertex to the vertex array
            v.add(Float.valueOf(tokens[i]));
        }
    }

    /**
     *parse normals
     * @param line - one line from file
     */
    private void processVNLine(String line){
        String [] tokens=line.split("[ ]+"); //split the line at the spaces
        int c=tokens.length;
        for(int i=1; i<c; i++){ //add the vertex to the vertex array
            vn.add(Float.valueOf(tokens[i]));
        }
    }

    /**
     *parse texturecoords
     * @param line - one line from file
     */
    private void processVTLine(String line){
        String [] tokens=line.split("[ ]+"); //split the line at the spaces
        int c=tokens.length;
        for(int i=1; i<c; i++){ //add the vertex to the vertex array
            vt.add(Float.valueOf(tokens[i]));
        }
    }

    /**
     * Parse faces
     * @param line - one line from file
     */
    private void processFLine(String line){
        String [] tokens=line.split("[ ]+");
        int c=tokens.length;

        if(tokens[1].matches("[0-9]+")){//f: v
            if(c==4){//3 faces
                for(int i=1; i<c; i++){
                    Short s=Short.valueOf(tokens[i]);
                    s--;
                    faces.add(s);
                }
            }
            else{//more faces
                Vector<Short> polygon=new Vector<Short>();
                for(int i=1; i<tokens.length; i++){
                    Short s=Short.valueOf(tokens[i]);
                    s--;
                    polygon.add(s);
                }
                faces.addAll(triangulate(polygon));//triangulate the polygon and add the resulting faces
            }
        }
        if(tokens[1].matches("[0-9]+/[0-9]+")){//if: v/vt
            if(c==4){//3 faces
                for(int i=1; i<c; i++){
                    Short s=Short.valueOf(tokens[i].split("/")[0]);
                    s--;
                    faces.add(s);
                    s=Short.valueOf(tokens[i].split("/")[1]);
                    s--;
                    vtPointer.add(s);
                }
            }
            else{//triangulate
                Vector<Short> tmpFaces=new Vector<Short>();
                Vector<Short> tmpVt=new Vector<Short>();
                for(int i=1; i<tokens.length; i++){
                    Short s=Short.valueOf(tokens[i].split("/")[0]);
                    s--;
                    tmpFaces.add(s);
                    s=Short.valueOf(tokens[i].split("/")[1]);
                    s--;
                    tmpVt.add(s);
                }
                faces.addAll(triangulate(tmpFaces));
                vtPointer.addAll(triangulate(tmpVt));
            }
        }
        if(tokens[1].matches("[0-9]+//[0-9]+")){//f: v//vn
            if(c==4){//3 faces
                for(int i=1; i<c; i++){
                    Short s=Short.valueOf(tokens[i].split("//")[0]);
                    s--;
                    faces.add(s);
                    s=Short.valueOf(tokens[i].split("//")[1]);
                    s--;
                    vnPointer.add(s);
                }
            }
            else{//triangulate
                Vector<Short> tmpFaces=new Vector<Short>();
                Vector<Short> tmpVn=new Vector<Short>();
                for(int i=1; i<tokens.length; i++){
                    Short s=Short.valueOf(tokens[i].split("//")[0]);
                    s--;
                    tmpFaces.add(s);
                    s=Short.valueOf(tokens[i].split("//")[1]);
                    s--;
                    tmpVn.add(s);
                }
                faces.addAll(triangulate(tmpFaces));
                vnPointer.addAll(triangulate(tmpVn));
            }
        }
        if(tokens[1].matches("[0-9]+/[0-9]+/[0-9]+")){//f: v/vt/vn

            if(c==4){//3 faces
                for(int i=1; i<c; i++){
                    Short s=Short.valueOf(tokens[i].split("/")[0]);
                    s--;
                    faces.add(s);
                    s=Short.valueOf(tokens[i].split("/")[1]);
                    s--;
                    vtPointer.add(s);
                    s=Short.valueOf(tokens[i].split("/")[2]);
                    s--;
                    vnPointer.add(s);
                }
            }
            else{//triangulate
                Vector<Short> tmpFaces=new Vector<Short>();
                Vector<Short> tmpVn=new Vector<Short>();
                for(int i=1; i<tokens.length; i++){
                    Short s=Short.valueOf(tokens[i].split("/")[0]);
                    s--;
                    tmpFaces.add(s);

                }
                faces.addAll(triangulate(tmpFaces));
                vtPointer.addAll(triangulate(tmpVn));
                vnPointer.addAll(triangulate(tmpVn));
            }
        }
    }

    // triangulate none triangular faces
    private Vector<Short> triangulate(Vector<Short> polygon){
        Vector<Short> triangles=new Vector<Short>();
        for(int i=1; i<polygon.size()-1; i++){
            triangles.add(polygon.get(0));
            triangles.add(polygon.get(i));
            triangles.add(polygon.get(i+1));
        }
        return triangles;
    }

    // Create a vector containing vertex data for glDrawArrays
    public void initVertices() {
        for(int i = 0; i < v.size(); i += 3) {
            float x = v.elementAt(i);
            float y = v.elementAt(i+1);
            float z = v.elementAt(i+2);

            vertexData.add(new Vector3f(x, y, z));
        }
    }

    // Create a vector containing texture coordinate data for glDrawArrays
    public void initTextureCoords() {
        for(int i = 0; i < vtPointer.size(); i++) {
            texturecoords.add(vt.elementAt(vtPointer.elementAt(i) * 2));
            texturecoords.add(vt.elementAt(vtPointer.elementAt(i) * 2 + 1));
        }
    }

    // Create a vector containing normal data for glDrawArrays
    public void initNormals() {
        for(int i = 0; i<vnPointer.size(); i += 3) {            // For each vertex
            float x, y, z;
            x = vn.elementAt(vnPointer.elementAt(i + 0) * 3 + 0);     // Normal's X
            y = vn.elementAt(vnPointer.elementAt(i + 1) * 3 + 1);     // Normal's Y
            z = vn.elementAt(vnPointer.elementAt(i + 2) * 3 + 2);     // Normal's Z
            normals.add(new Vector3f(x, y, z));
            normals.add(new Vector3f(x, y, z));
            normals.add(new Vector3f(x, y, z));
        }
    }

    // Getters
    public Vector<Short> getFaces() {
        return faces;
    }
    public Vector<Vector3f> getVertices() {
        return vertexData;
    }
    public Vector<Vector3f> getNormals() {
        return normals;
    }
    public Vector<Float> getTextureCoords() {
        return texturecoords;
    }
}
