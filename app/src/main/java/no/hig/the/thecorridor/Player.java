package no.hig.the.thecorridor;

import android.content.Context;

import com.google.vrtoolkit.cardboard.EyeTransform;


/**
 * Contains the player mesh and updates the position and animation
 */
public class Player {
    private final String PLAYER_FILE_NAME_ONE = "crow1";
    private final String PLAYER_FILE_NAME_TWO = "crow2";
    private final String PLAYER_FILE_NAME_THREE = "crow3";
    private final static float SCALE = 0.03f;
    private final int ANIMATION_STAGES = 3;
    private final int ANIMATION_DELAY = 10;
    private final int W = 3;
    int frameCount;
    int currentFrame;

    private Mesh player[];

    public Player(Context context, Camera camera, int textureHandler, int shaderProgram){
        player = new Mesh[ANIMATION_STAGES];
        //load the different meshes for animation and create them:
        OBJLoader objLoaderOne = new OBJLoader();
        objLoaderOne.parseOBJ(PLAYER_FILE_NAME_ONE, context);
        player[0] = new Mesh(objLoaderOne, textureHandler, shaderProgram);

        OBJLoader objLoaderTwo = new OBJLoader();
        objLoaderTwo.parseOBJ(PLAYER_FILE_NAME_TWO, context);
        player[1] = new Mesh(objLoaderTwo, textureHandler, shaderProgram);

        OBJLoader objLoaderThree = new OBJLoader();
        objLoaderThree.parseOBJ(PLAYER_FILE_NAME_THREE, context);
        player[2] = new Mesh(objLoaderThree, textureHandler, shaderProgram);

        float increaseY = 1.0f;
        float[] camPos = camera.getPosition();
        float[] scaleVector = {SCALE,SCALE,SCALE};
        camPos[W] = camPos[W] + increaseY;

        //set the position and scale:
        for (int i = 0; i < ANIMATION_STAGES; i++){
            player[i].transformation.setPosition(camPos);
            player[i].transformation.setScale(scaleVector);
        }

        currentFrame = 0;
        frameCount = 0;
    }

    /**
     * Updates the player with the position of the camera
     * @param camera the camera for the position
     */
    public void update(Camera camera){
        player[0].transformation.setPosition(camera.getViewDirectionPosition());
        player[1].transformation.setPosition(player[0].getPosition());
        player[2].transformation.setPosition(player[0].getPosition());
    }

    /**
     * Handles the animation for the player
     * @param eyeTransform the transformations to apply to an eye
     * @param view the view matrix
     */
    public void playAnimation(EyeTransform eyeTransform, float[] view) {
        frameCount--;
        if(frameCount < 0) {
            frameCount = ANIMATION_DELAY;
            currentFrame++;
        }

        if(currentFrame >= ANIMATION_STAGES)
            currentFrame = 0;

        player[currentFrame].update(view, eyeTransform);
        player[currentFrame].draw();
    }

    /**
     * @return the mesh for player
     */
    public Mesh getMainMesh() {
        return player[0];
    }


    /**
     * Free GLES memory used by all the player meshes
     */
    public void freePlayer() {
        for(Mesh pose : player) {
            pose.freeMemory();
        }
    }
}
