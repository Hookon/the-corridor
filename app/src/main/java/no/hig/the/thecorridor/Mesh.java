package no.hig.the.thecorridor;

import android.opengl.GLES20;
import android.opengl.Matrix;
import com.google.vrtoolkit.cardboard.EyeTransform;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import java.util.Vector;


/**
 * Mesh class, containing everything needed for drawing, texture coordinates, vertex position etc.
 */
public class Mesh {

    private FloatBuffer vertexBuffer;
    private FloatBuffer normalBuffer;
    private FloatBuffer textureBuffer;

    private Vector<Vector3f> vertices;
    private Vector<Vector3f> normals;
    private Vector<Short> indecies;
    private Vector<Float> textureCoords;

    protected float[] mModelViewProjection;
    private float[] size;
    protected float mHead[];

    private int textureUniform;
    private int textCoordsHandle;
    private int textureHandle;

    private int posHandle;
    private int normalHandle;

    private int mvpHandle;
    private int mmHandle;

    protected Transformation transformation;

    public Mesh(OBJLoader objLoader, int textureHandle, int program) {
        //setting our data:
        this.textureHandle = textureHandle;
        this.vertices = objLoader.getVertices();
        this.indecies = objLoader.getFaces();
        this.textureCoords = objLoader.getTextureCoords();
        this.normals = objLoader.getNormals();

        transformation = new Transformation();
        mModelViewProjection = new float[16];

        //filling our vertex buffer:
        ByteBuffer vb = ByteBuffer.allocateDirect(indecies.size() * vertices.firstElement().getNUM_COMPONENTS() * 4);
        vb.order(ByteOrder.nativeOrder());
        vertexBuffer = vb.asFloatBuffer();
        for(int i = 0; i<indecies.size(); i++) {
            vertexBuffer.put(vertices.elementAt(indecies.elementAt(i)).getXYZ());
        }
        vertexBuffer.position(0);

        //filling our normal buffer:
        ByteBuffer nb = ByteBuffer.allocateDirect(normals.size() * normals.firstElement().getNUM_COMPONENTS() * 4);
        nb.order(ByteOrder.nativeOrder());
        normalBuffer = nb.asFloatBuffer();
        for(int i = 0; i<normals.size(); i++) {
            normalBuffer.put(normals.elementAt(i).getXYZ());
        }
        normalBuffer.position(0);

        //Filling our texture coordinate buffer:
        ByteBuffer tcbb = ByteBuffer.allocateDirect(textureCoords.size() * 4);
        tcbb.order(ByteOrder.nativeOrder());
        textureBuffer = tcbb.asFloatBuffer();
        for(int i = 0; i<textureCoords.size(); i++) {
            textureBuffer.put(textureCoords.elementAt(i));
        }
        textureBuffer.position(0);

        //Set the handles:
        posHandle = GLES20.glGetAttribLocation(program, "vPosition");
        normalHandle = GLES20.glGetAttribLocation(program, "vNormal");
        textCoordsHandle = GLES20.glGetAttribLocation(program, "texCoord");
        textureUniform = GLES20.glGetUniformLocation(program, "u_texture");
        mvpHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix");
        mmHandle = GLES20.glGetUniformLocation(program, "uModelMatrix");

        //For storing the coordinates for the bounding box:
        float max_x = 0.0f,
              min_x = 0.0f,

              max_y = 0.0f,
              min_y = 0.0f,

              max_z = 0.0f,
              min_z = 0.0f;

        // Calculate bounding box/size of mesh:
        for(int i = 0; i < vertices.size(); i++) {
            if (vertices.elementAt(i).getX() < min_x) min_x = vertices.elementAt(i).getX();
            if (vertices.elementAt(i).getX() > max_x) max_x = vertices.elementAt(i).getX();
            if (vertices.elementAt(i).getY() < min_y) min_y = vertices.elementAt(i).getY();
            if (vertices.elementAt(i).getY() > max_y) max_y = vertices.elementAt(i).getY();
            if (vertices.elementAt(i).getZ() < min_z) min_z = vertices.elementAt(i).getZ();
            if (vertices.elementAt(i).getZ() > max_z) max_z = vertices.elementAt(i).getZ();
        }

        //stores the bounding box
        // With (X), height(Y), depth(Z)
        size = new float[] {max_x-min_x, max_y-min_y, max_z-min_z };
        mHead = new float[16];
    }

    /**
     * Draws the mesh
     */
    public void draw() {
        //set the active texture:
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        //bind the texture:
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle);
        //set the texture for the shader:
        GLES20.glUniform1i(textureUniform, 0);

        //Telling the gpu where to get the normals:
        GLES20.glEnableVertexAttribArray(normalHandle);
        GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false, 0, normalBuffer);

        //Telling the gpu where to get the vertex positions:
        GLES20.glEnableVertexAttribArray(posHandle);
        GLES20.glVertexAttribPointer(posHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);

        //Telling the gpu where to get the texture coordinates:
        GLES20.glEnableVertexAttribArray(textCoordsHandle);
        GLES20.glVertexAttribPointer(textCoordsHandle, 2, GLES20.GL_FLOAT, false, 0, textureBuffer);

        //Sending the MVP and model matrix to the shader:
        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mModelViewProjection, 0);
        GLES20.glUniformMatrix4fv(mmHandle, 1, false, getModel(), 0);

        //Draw the vertexes as triangles:
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, indecies.size());
    }

    /**
     * update the MVP matrix:
     * @param mView the view matrix
     * @param eyeTransform the transformations to apply to an eye
     */
    public void update(float[] mView, EyeTransform eyeTransform) {
        float[] mModelView = new float[16];

        Matrix.multiplyMM(mModelView, 0, mView, 0, getModel(), 0);

        Matrix.multiplyMM(mModelViewProjection, 0, eyeTransform.getPerspective(), 0, mModelView, 0);
    }

    // Frees the bytebuffers used by OpenGL ES
    public void freeMemory() {
        int[] buffer = new int[1];

        buffer[0] = normalHandle;
        GLES20.glDeleteBuffers(1, buffer, 0);

        buffer[0] = posHandle;
        GLES20.glDeleteBuffers(1, buffer, 0);

        buffer[0] = textCoordsHandle;
        GLES20.glDeleteBuffers(1, buffer, 0);

        buffer[0] = textureHandle;
        GLES20.glDeleteTextures(1, buffer, 0);
    }

    /**
     * @return the model Matrix
     */
    public float[] getModel() {
        return transformation.getModel();
    }

    /**
     * @return return the position:
     */
    public float[] getPosition() {
        return transformation.getPosition();
    }

    /**
     * @return the real size of the mesh with transformation:
     */
    public float[] getRealSize() {

        float[] realSize = new float[3];

        for(int i = 0; i < size.length; i++) {
            realSize[i] = size[i] * transformation.getScale()[i];
        }
        return realSize;
    }

    /**
     * send in new transformation on the Mesh:
     * @param transformation the transformation for the mesh
     */
    public void setTransformation(Transformation transformation) {  this.transformation = transformation; }

    /**
     * retrieves the transformation for current mesh:
     * @return the transformation
     */
    public Transformation getTransformation() { return transformation; }

}