package no.hig.the.thecorridor;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


/**
 * Class for getting accelerometer data
 */
public class SensorInputHandler extends Activity implements SensorEventListener {
    private final float ACC_SCALE_FACTOR = 0.01f;           // Scale acc data
    private float accX;
    private Sensor sensor;

    /**
     * Init accelerometer and starts a listener
     * @param context - Context
     */
    public SensorInputHandler(Context context) {
        SensorManager manager = (SensorManager)context.getSystemService(SENSOR_SERVICE);

        // Get sensor
        if(manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
            sensor = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);

        // Start listening for data
        manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // Get acceleration in x-axis
        accX = sensorEvent.values[1] * ACC_SCALE_FACTOR;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * Getter for accX
     * @return - accX
     */
    public float getAccX() {
        return accX;
    }
}
