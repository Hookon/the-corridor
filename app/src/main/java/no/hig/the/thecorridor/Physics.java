package no.hig.the.thecorridor;

import android.content.Context;

/**
 * This class takes in two meshes and checks distance and collision between the two
 */
public class Physics {

    Context context;
    static float[] meshTwoPosition = new float[3];
    static float[] meshOnePosition = new float[3];
    static float[] meshTwoRealSize = new float[3];
    static float[] meshOneRealSize = new float[3];

    final static float MIN_DISTANCE_Y = 0.5f;
    public Physics(Context context){
        this.context = context;


    }

    public static boolean checkDistanceZ(Mesh meshOne, Mesh meshTwo){
        meshOnePosition = meshOne.getTransformation().getPosition();
        meshTwoPosition = meshTwo.getTransformation().getPosition();

        float meshTwoMinZ = meshTwoPosition[2] - (meshTwoRealSize[2] / 2);
        float meshOneMaxZ = meshOnePosition[2] + (meshOneRealSize[2] / 2);

        if (Math.abs(meshTwoMinZ - meshOneMaxZ) < MIN_DISTANCE_Y){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Checks if bounding boxes of two meshes are colliding.
     * (this is currently only working for non-rotated meshes)
     *
     * @param meshOne The first mesh
     * @param meshTwo The other mesh
     * @param margin Crash margin/tolerance for the first Mesh. 0 for default
     * @return
     */
   public static boolean checkCollision(Mesh meshOne, Mesh meshTwo, float margin){
        float[] meshTwoPosition;
        float[] meshOnePosition;
        float[] meshTwoRealSize;
        float[] meshOneRealSize;

        meshTwoRealSize = meshTwo.getRealSize();
        meshOneRealSize = meshOne.getRealSize();

        meshTwoPosition = meshTwo.getTransformation().getPosition();
        meshOnePosition = meshOne.getTransformation().getPosition();

        float meshTwoMaxX = meshTwoPosition[0] + (meshTwoRealSize[0] / 2);
        float meshTwoMinX = meshTwoPosition[0] - (meshTwoRealSize[0] / 2);
        float meshTwoMaxY = meshTwoPosition[1] + (meshTwoRealSize[1] / 2);
        float meshTwoMinY = meshTwoPosition[1] - (meshTwoRealSize[1] / 2);
        float meshTwoMaxZ = meshTwoPosition[2] + (meshTwoRealSize[2] / 2);
        float meshTwoMinZ = meshTwoPosition[2] - (meshTwoRealSize[2] / 2);

        float meshOneMaxX = meshOnePosition[0] + (meshOneRealSize[0] / 2);
        float meshOneMinX = meshOnePosition[0] - (meshOneRealSize[0] / 2);
        float meshOneMaxY = meshOnePosition[1] + (meshOneRealSize[1] / 2);
        float meshOneMinY = meshOnePosition[1] - (meshOneRealSize[1] / 2);
        float meshOneMaxZ = meshOnePosition[2] + (meshOneRealSize[2] / 2);
        float meshOneMinZ = meshOnePosition[2] - (meshOneRealSize[2] / 2);

        return(meshTwoMaxX > meshOneMinX + margin &&
                meshTwoMinX < meshOneMaxX - margin &&
                meshTwoMaxY > meshOneMinY + margin &&
                meshTwoMinY < meshOneMaxY - margin &&
                meshTwoMaxZ > meshOneMinZ + margin &&
                meshTwoMinZ < meshOneMaxZ - margin);

    }

    /**
     *
     * @param corridorPart The corridor(part) to check collision with
     * @param player The player
     * @return Which direction(s) (X or/and Y) the player is colliding
     */
    public static boolean[] checkCorridorCollision(Mesh corridorPart, Player player) {
        boolean[] wallsCollidedWith = { false, false };

        int x = 0;
        int y = 1;

        float[] playerSize = player.getMainMesh().getRealSize();
        float[] corrSize = corridorPart.getRealSize();

        float leftWall = corridorPart.getPosition()[x] - corrSize[x]/2;
        float rightWall = corridorPart.getPosition()[x] + corrSize[x]/2;
        float roof = corridorPart.getPosition()[y] + corrSize[y]/2;
        float floor = corridorPart.getPosition()[y] - corrSize[y]/2;

        if(player.getMainMesh().transformation.getPosition()[x] + playerSize[x]/2 > rightWall) {
            wallsCollidedWith[x] = true;
        }
        if(player.getMainMesh().transformation.getPosition()[x] - playerSize[x]/2 < leftWall) {
            wallsCollidedWith[x] = true;
        }
        if(player.getMainMesh().transformation.getPosition()[y] + playerSize[y]/2 > roof) {
            wallsCollidedWith[y] = true;
        }
        if((player.getMainMesh().transformation.getPosition()[y]) - playerSize[y]/2 < floor) {
            wallsCollidedWith[y] = true;
        }
        return wallsCollidedWith;
    }

}
