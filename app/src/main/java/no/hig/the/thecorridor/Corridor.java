package no.hig.the.thecorridor;

import android.content.Context;

import com.google.vrtoolkit.cardboard.EyeTransform;

/**
 * Our corridor container class, contains all the corridor parts
 */
public class Corridor {
    private final String corridorFileName = "corridor.obj";
    private final int NUM_CORRIDOR_PARTS = 20;
    private final int WAIT_TIME = 25;

    private float corridorPartLength;
    private int counter;
    private float offsetCorridorBehindPlayer = 2.0f;
    private int z = 2;

    private Texture corrTexture;

    Mesh[] corridorParts;

    int firstCorrIndex;     // The index of the corridor part that currently is in the end of the corridor
    float lastCorrZpos;     //The z position of the last corridor part

    public Corridor(Context context, int shaderProgram) {

        //create the object loader and loading the corridor:
        OBJLoader objLoader = new OBJLoader();
        objLoader.parseOBJ(corridorFileName, context);
        //create the texture:
        corrTexture = new Texture(context, R.drawable.corridortexture);
        //create the corridorParts array:
        corridorParts = new Mesh[NUM_CORRIDOR_PARTS];

        // Create and place some corridor parts:
        for(int i = 0; i < NUM_CORRIDOR_PARTS; i++) {

            Mesh part = new Mesh(objLoader, corrTexture.getTextureHandler(), shaderProgram);
            corridorPartLength = part.getRealSize()[z];
            // Place the corridor parts in front of the camera, the and a few corridors  behind
            part.transformation.setPosition(new float[] { 0, 0, (NUM_CORRIDOR_PARTS / offsetCorridorBehindPlayer) - corridorPartLength * i});
            corridorParts[i] = part;
        }

        firstCorrIndex = 0;
        counter = 0;
        //set the last corridors z position:
        lastCorrZpos = (NUM_CORRIDOR_PARTS - 1) * corridorPartLength;
    }

    /**
     * moves a corridor part forward and updates the different parts of our corridor
     * @param cameraPosition the position of the camera
     * @param mView the viewMatrix
     * @param eyeTransform the transformations to apply to an eye
     * @return if we are to place a new obstacle or not
     */
    public boolean update(float[] cameraPosition, float[] mView, EyeTransform eyeTransform ) {
        boolean placeNewObject = false;

        // Z position of camera is more than the Z pos of the middle corridor part
        if(cameraPosition[z] < (lastCorrZpos + (NUM_CORRIDOR_PARTS/offsetCorridorBehindPlayer) * corridorPartLength)) {
            // Move first corridor forward
            corridorParts[firstCorrIndex].transformation.setPosition(new float[] { 0, 0, lastCorrZpos -= corridorPartLength });
            firstCorrIndex++;

            // Reset index counter
            if(firstCorrIndex == NUM_CORRIDOR_PARTS)
                firstCorrIndex = 0;

            //check if we are to start placing the obstacles:
            if (counter == WAIT_TIME ) {
                placeNewObject = true;
            }
            else{
                counter++;
            }
        }

        // Update the corridor parts (calc. MVP)
        for (int i = 0; i < NUM_CORRIDOR_PARTS; i++)
            corridorParts[i].update(mView, eyeTransform);

        return placeNewObject;
    }

    /**
     * Draw the corridor parts:
     */
    public void draw() {
        for(int i = 0; i < NUM_CORRIDOR_PARTS; i++) {
            corridorParts[i].draw();
        }
    }

    /**
     * @return the last corridor parts z position:
     */
    public float getLastCorrZpos() { return lastCorrZpos;}

    /**
     * Free GLES memory used by all the corridor meshes
     */
    public void freeCorridors() {
        for(Mesh part : corridorParts)
            part.freeMemory();
    }
}
