package no.hig.the.thecorridor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Looper;

import com.google.vrtoolkit.cardboard.EyeTransform;
import com.google.vrtoolkit.cardboard.HeadTransform;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This contains all main functions and objects needed for our game, and this is where
 * everything in the game is managed.
 */
public class Game {

    private final float MOVE_PLAYER_BACK_X = 0.001f;    // Used to move player back after hitting max/min x value
    private final float MAX_X_PLAAYER = 0.9f;           // Used to restrict player movments in x-direction
    private final float DECREASE_LIFE = 0.01f;          // Decrease player life every  TIMER_TIME in milliseconds
    private final float INCREASE_LIFE = 0.3f;           // Increase player life every  TIMER_TIME in milliseconds
    private final float INCREASE_SCORE = 0.10f;         // Increase score  TIMER_TIME in milliseconds
    private final int TIMER_TIME = 100;                 // Timer updates life and score
    private final int TIMER_DELAY = 5000;               // Delay before timer starts and life and score is updated

    private final float[] lightDirection = {0, 0, -1};  // Always point light in -Z direction (into the corridor)

    private Context context;

    private Shader environmentShader;                   // Shader used to draw models with spot light
    private Shader playerShader;                        // Shader used to draw models with texture without spot light

    private Camera camera;

    private Player player;
    private Texture playerTexture;

    private Corridor corridor;
    private Obstacles obstacles;
    private Foods foods;

    private MediaPlayer eatPlayer;
    private MediaPlayer crashPlayer;

    private SensorInputHandler sensor;                  // Used to move the player in x-axis
    private Timer timer;                                // Used to update score and life every TIMER_TIME

    private float life;
    private float score;

    private float[] view;
    private float[] lightColor;


    /**
     * Initialises main objects
     * @param context - Activity context
     */
    public Game(Context context) {
        this.context = context;
        Looper.prepare();

        lightColor = new float[4];
        view = new float[16];
        camera = new Camera();

        sensor = new SensorInputHandler(context);
        eatPlayer = MediaPlayer.create(context, R.raw.potetgull);
        crashPlayer = MediaPlayer.create(context, R.raw.crash);

        environmentShader = new Shader(context, R.raw.vertexshader, R.raw.fragmentshader);
        playerShader = new Shader(context, R.raw.texture_only_vertex, R.raw.texture_only_fragment);

        playerTexture = new Texture(context, R.drawable.crow);
        player = new Player(context, camera, playerTexture.getTextureHandler(), playerShader.getProgramHandle());
        corridor = new Corridor(context, environmentShader.getProgramHandle());
        obstacles = new Obstacles(context, environmentShader.getProgramHandle());
        foods = new Foods(context, environmentShader.getProgramHandle());

        score = 0;
        life = 1;

        // Timer is created and started. Cant start the timer before the game is up and running
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                life -= DECREASE_LIFE;
                score += INCREASE_SCORE;
            }
        }, TIMER_DELAY, TIMER_TIME);
    }

    /**
     * Update the camera and player position
     */
    public void updateGame(HeadTransform headTransform) {
        // Update camera forward vector
        camera.updateHeadTracking(headTransform);

        if(life < 0)                // If out of life
            exitGame();             // Go back to main menu

        if(life > 1.0f)             // Keep life at max 1 (life goes from 0 to 1)
            life = 1.0f;

        moveCamera();               // Move camera
        player.update(camera);      // Move player according to camera
    }

    /**
     * Moves camera in a valid direction while locking it to the corridor
     */
    private void moveCamera() {
        // x y z index to vectors
        int x = 0;
        int y = 1;
        int z = 2;

        // Check collision between player and foods
        for (int i = 0; i < foods.NUM_SPIDERS; i++) {
            if (Physics.checkDistanceZ(foods.getSpider(i).getMesh(), player.getMainMesh())) {
                if (Physics.checkCollision(foods.getSpider(i).getMesh(), player.getMainMesh(), 0.0f)){
                    float pos[] =  foods.getSpider(i).getMesh().getTransformation().getPosition();
                    pos[2] += 10;
                    foods.getSpider(i).getMesh().getTransformation().setPosition(pos);
                    score++;
                    life += INCREASE_LIFE;
                    eatPlayer.start();
                }
            }
        }

        // Check collision between player and timbers
        for(int i = 0; i<obstacles.NUM_TIMBERS; i++) {
            if (Physics.checkDistanceZ(obstacles.getWood(i), player.getMainMesh())){
                if (Physics.checkCollision(obstacles.getWood(i), player.getMainMesh(), 0.1f)){
                    exitGame();
                }
            }
        }

        // Check for collision with corridor walls
        boolean[] walls = Physics.checkCorridorCollision(corridor.corridorParts[0], player);
        if(walls[x] == true && walls[y] == true)
            camera.moveInDirection(new float[] {0, 0, camera.getViewDirection()[z]});                               // Collision in both x and y axis (a corner):
        else if(walls[x] == true)
            camera.moveInDirection(new float[] {0, camera.getViewDirection()[y], camera.getViewDirection()[z]});    // Collision in x axis (with the walls)
        else if(walls[y] == true)
            camera.moveInDirection(new float[] {camera.getViewDirection()[x], 0, camera.getViewDirection()[z]});    // Collision in y axis (with roof ar walls)
        else
            camera.forward();                                                                                       // No collision

        // move player in x directions
        if(camera.getPosition()[0] < MAX_X_PLAAYER && camera.getPosition()[0] > -MAX_X_PLAAYER)
            camera.setPosition(new float[]{camera.getPosition()[0] + sensor.getAccX(), camera.getPosition()[1], camera.getPosition()[2], camera.getPosition()[3]});
        else {
            if(camera.getPosition()[0] < MAX_X_PLAAYER)
                camera.setPosition(new float[]{camera.getPosition()[0] + MOVE_PLAYER_BACK_X, camera.getPosition()[1], camera.getPosition()[2], camera.getPosition()[3]});
            else
                camera.setPosition(new float[]{camera.getPosition()[0] - MOVE_PLAYER_BACK_X, camera.getPosition()[1], camera.getPosition()[2], camera.getPosition()[3]});
        }
    }


    /**
     * Render/draw the scene
     * @param eyeTransform The eye to render for
     */
    public void render(EyeTransform eyeTransform) {

        // Applying eyeTransformations to the view matrix
        Matrix.multiplyMM(view, 0, eyeTransform.getEyeView(), 0, camera.getViewMatrix(), 0);

        // Draw the player using a shader without lighting
        playerShader.bind();
        player.playAnimation(eyeTransform, view);   // Update animation and draw

        // Select shader
        environmentShader.bind();

        lightColor[0] = 1.3f-life;
        lightColor[1] = 0.3f;
        lightColor[2] = 0.3f;
        lightColor[3] = 1.0f;

        // Update light uniforms
        environmentShader.updateLightColor(lightColor);
        environmentShader.updateAmbientLight(lightColor);
        environmentShader.update(player.getMainMesh().getPosition(), camera.getPosition(), lightDirection);

        // Check if spider is behind camera position, if so it is moved
        foods.update(camera.getPosition());

        // Check if a corridor part is behind the camera and repositioning them.
        if (corridor.update(camera.getPosition(),view,eyeTransform)){
            // If the corridor is repositioned, reposition the obstacle too
            obstacles.placeObstacle(corridor.getLastCorrZpos());
        }

        corridor.draw();                            // Draw the corridor
        foods.draw(eyeTransform, view);             // Draw foods(spiders)
        obstacles.draw(eyeTransform, view);         // Draw all obstacles
    }

    /**
     * Go back to menu
     */
    public void exitGame() {

        Intent intent = new Intent(context, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("score", String.valueOf(((int)score)));

        crashPlayer.start();
        crashPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                crashPlayer.release();
                crashPlayer = null;
            }
        });
        timer.cancel();
        eatPlayer.release();
        eatPlayer = null;

        context.startActivity(intent);
        ((Activity)context).finish();
    }

    /**
     * Release the (OpenGL ES) resources used by our game
     */
    public void freeResources() {
        corridor.freeCorridors();
        foods.freeFoods();
        obstacles.freeObstacles();
        player.freePlayer();

        GLES20.glDeleteProgram(environmentShader.getProgramHandle());
        GLES20.glDeleteProgram(playerShader.getProgramHandle());
    }

    public Texture getPlayerTexture() {
        return playerTexture;
    }

    public Player getPlayer() {
        return  player;
    }
}
