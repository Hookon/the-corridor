package no.hig.the.thecorridor;

import android.content.Context;

import com.google.vrtoolkit.cardboard.EyeTransform;

import java.util.Random;


/**
 * The obstacle container class, creates them and places them
 */
public class Obstacles {
    public final int NUM_TIMBERS = 20;

    private final String woodFileHorizontal = "wood_hor.obj";
    private final String woodFileVertical = "wood_vert.obj";

    private final float[] offsetStartPos = new float[] {0,0,5};
    private final float[] scale = {1.0f,1.0f,1.0f};

    private final int offset = 1;

    private Mesh[] woods;

    private Random random;

    private int lastUsedIndex;

    Texture woodTexture;

    public Obstacles(Context context, int shaderProgram){
        //create the object loaders:
        OBJLoader objLoaderOne = new OBJLoader();
        OBJLoader objLoaderTwo = new OBJLoader();
        //Create the obstacle array and texture:
        woods = new Mesh[NUM_TIMBERS];
        woodTexture = new Texture(context, R.drawable.wood);
        //Parse the obj files:
        objLoaderOne.parseOBJ(woodFileHorizontal, context);
        objLoaderTwo.parseOBJ(woodFileVertical, context);
        //Fill the obstacle array:
        for (int i = 0; i < NUM_TIMBERS; i++){
            if (i % 2 == 0) {
                woods[i] = new Mesh(objLoaderOne, woodTexture.getTextureHandler(), shaderProgram);

            }else {
                woods[i] = new Mesh(objLoaderTwo, woodTexture.getTextureHandler(), shaderProgram);
            }
            woods[i].getTransformation().setScale(scale);
            woods[i].getTransformation().setPosition(offsetStartPos);
        }

        lastUsedIndex = 0;

        random = new Random();

        random.setSeed(50);
    }

    /**
     * place the obstacles using the z from the last placed corridor:
     * @param lastZPos ,last corridor parts z position
     */
    public void placeObstacle(float lastZPos){
        float[] newPosition = {0.0f,0.0f,0.0f};
        int x = 0;
        int y = 1;
        int z = 2;
        //set the position:
        newPosition[x] = calculateXorY();
        newPosition[y] = calculateXorY();
        newPosition[z] = lastZPos + offset;
        woods[lastUsedIndex].transformation.setPosition(newPosition);

        lastUsedIndex++;
        //Check if all obstacles have been placed:
        if(lastUsedIndex == NUM_TIMBERS){
            lastUsedIndex = 0;
        }
    }

    /**
     * draw all obstacles
     *
     * @param mView - view matrix
     * @param eyeTransform - EyeTransform used to get eye view matrix to rotate the camera view.
     */
    public void draw(EyeTransform eyeTransform, float[] mView) {
        for (int i = 0; i < NUM_TIMBERS; i++) {
            woods[i].update(mView, eyeTransform);
            woods[i].draw();
        }
    }

    /**
     * find the x or y value for positioning the obstacles:
     * @return returns the value for x or y
     */
    public float calculateXorY(){
        final float oneQuarter = 0.25f;
        final float threeQuarter = 0.75f;
        final float negative =-1.0f;
        final int modValue = 2;

        //decide if we are placing it in positive or negative direction in the coordinate system:
        switch ((random.nextInt() % modValue) + offset){
            //decide how far we are placing them:
            case 1: switch ((random.nextInt() % modValue) + offset){     //Negativ
                case 1: return (threeQuarter * (negative));
                case 2: return (oneQuarter * (negative));
            }
                break;
            //decide how far we are placing them:
            case 2: switch ((random.nextInt() % modValue)+offset){    //positiv
                case 1: return threeQuarter;
                case 2: return oneQuarter;
            }
                break;
        }
        return 0;
    }

    /**
     * gets the Mesh
     * @param index what obstacle mesh we want
     * @return the mesh
     */
    public Mesh getWood(int index){return woods[index];};

    /**
     * Free GLES memory used by all the obstacles
     */
    public void freeObstacles() {
        for(Mesh obstacle : woods) {
            obstacle.freeMemory();
        }
    }
}
