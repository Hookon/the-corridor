package no.hig.the.thecorridor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

/**
 *  Class for creating and storing texturehandle
 */
public class Texture {
    private int textureHandler;

    /**
     * Create a texture
     *
     * @param context - context
     * @param type - img id
     */
    public Texture(Context context, int type) {
        final int[] textureHandle0 = new int[1];

        // Create texture
        GLES20.glGenTextures(1, textureHandle0, 0);

        if (textureHandle0[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;

            // Read in the resource
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), type, options);
            if (bitmap == null) {
                throw new RuntimeException("Error decoding bitmap");
            }
            // Bind to the texture in OpenGL
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle0[0]);

            // Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);

            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            // Recycle the bitmap, since its data has been loaded into OpenGL.
            bitmap.recycle();

        }

        if (textureHandle0[0] == 0)
            throw new RuntimeException("Error loading texture.");

        this.textureHandler = textureHandle0[0];
    }

    // Get texture handle
    public int getTextureHandler() {
        return textureHandler;
    }

}
