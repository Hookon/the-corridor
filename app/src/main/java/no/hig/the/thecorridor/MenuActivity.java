package no.hig.the.thecorridor;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.vrtoolkit.cardboard.CardboardDeviceParams;
import com.google.vrtoolkit.cardboard.sensors.MagnetSensor;
import com.google.vrtoolkit.cardboard.sensors.NfcSensor;

/*
        ******************************** Made by THE **************************************
        *                                                                                 *
        *    -------------------------------------------------------------------------    *
        *    | Group members | Name                      | student nr   |   Class    |    *
        *    -------------------------------------------------------------------------    *
        *    |               | Håkon Bjørklund           |    121059    | 12HBSPA    |    *
        *    |               | Even Arneberg Rognlien    |    120649    | 12HBIDATA  |    *
        *    |               | Tien Tran                 |    120647    | 12HBIDATA  |    *
        *    -------------------------------------------------------------------------    *
        *                                                                                 *
        *             /////////////////  ||         ||   ///////////                      *
        *                     ||         ||         ||   //                               *
        *                     ||         ||         ||   //                               *
        *                     ||         ||/////////||   //////                           *
        *                     ||         ||         ||   //                               *
        *                     ||         ||         ||   //                               *
        *                     ||         ||         ||   ////////////                     *
        *                                                                                 *
        ***********************************************************************************
*/


public class MenuActivity extends Activity {

    Point point;
    Display display;

    private final float MIDDLE_X = 2.5f;
    private final float Y_OFFSET_INSTRUCTION = 2.1f;
    private final float Y_OFFSET_NEW_GAME = 3.5f;
    private final float Y_OFFSET_QUIT_GAME = 1.5f;
    private final float Y_OFFSET_VRMODE_ENABLE = 1.15f;
    private final float Y_OFFSET_VRMODE_ENABLE_TEXT = 1.23f;
    private final int SCORE_OFFSET = 10;

    private  Intent gameActivityIntent;

    private MagnetSensor magnetSensor;
    private NfcSensor nfcSensor;

    private boolean vrMode;
    String score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        gameActivityIntent = new Intent(this, GameActivity.class);
        magnetSensor = new MagnetSensor(this);
        nfcSensor = NfcSensor.getInstance(this);

        //Get the screen size:
        point = new Point();
        display = getWindowManager().getDefaultDisplay();
        display.getSize(point);

        //Create the button and place them:
        final Button startGame = new Button(this);
        startGame.setText(R.string.New_Game);
        startGame.setX((int)(point.x/MIDDLE_X));
        startGame.setY((int)(point.y/Y_OFFSET_NEW_GAME));

        Button instructions = new Button(this);
        instructions.setText(R.string.Instructions);
        instructions.setX((int)(point.x/MIDDLE_X));
        instructions.setY((int)(point.y/Y_OFFSET_INSTRUCTION));

        Button endGame = new Button(this);
        endGame.setText(R.string.Quit_Game);
        endGame.setX((int)(point.x/MIDDLE_X));
        endGame.setY((int)(point.y/Y_OFFSET_QUIT_GAME));

        final Switch vrModeEnabled = new Switch(this);
        vrModeEnabled.setX((int)(point.x/MIDDLE_X));
        vrModeEnabled.setY((int)(point.y/Y_OFFSET_VRMODE_ENABLE));
        vrModeEnabled.setChecked(true);
        vrMode = true;

        TextView vrModeEnabledTextView = new TextView(this);
        vrModeEnabledTextView.setText(R.string.VRMode);
        vrModeEnabledTextView.setTextColor(Color.BLACK);
        vrModeEnabledTextView.setX((int)(point.x/MIDDLE_X));
        vrModeEnabledTextView.setY((int)(point.y/Y_OFFSET_VRMODE_ENABLE_TEXT));

        //Create layout parameters:
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        //Add the buttons:
        addContentView(startGame, lp);
        addContentView(endGame,lp);
        addContentView(instructions,lp);
        addContentView(vrModeEnabled, lp);
        addContentView(vrModeEnabledTextView, lp);

        //Get the score if applicable:
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            TextView scoreView = new TextView(this);
            score = extras.getString("score");
            scoreView.setText(R.string.YourScore );
            scoreView.append(score);
            scoreView.setX((int) (point.x / MIDDLE_X)+SCORE_OFFSET);
            scoreView.setY((point.y / 6));
            scoreView.setTextColor(getResources().getColor(android.R.color.black));
            addContentView(scoreView, lp);
        }

        //Create the listeners for the buttons:
        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame();
            }
        });
        endGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        instructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInstructions();
            }
        });
        vrModeEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {vrMode = b;    }
        });

        // Detect magnetic pulls
        if(magnetSensor != null) {
            magnetSensor.setOnCardboardTriggerListener(new MagnetSensor.OnCardboardTriggerListener() {
                @Override
                public void onCardboardTrigger() {
                    startGame();
                }
            });
            magnetSensor.start();
        }

        // Add NFC sensor listener for removing and inserting mobile into cardboard
        if(nfcSensor.isNfcSupported() && nfcSensor.isNfcEnabled()) {
            nfcSensor.addOnCardboardNfcListener(new NfcSensor.OnCardboardNfcListener() {
                @Override
                public void onInsertedIntoCardboard(CardboardDeviceParams cardboardDeviceParams) {
                    // Start game in VR mode
                    vrModeEnabled.setChecked(true);
                    startGame();
                }
                @Override
                public void onRemovedFromCardboard() {}
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(magnetSensor != null)
            magnetSensor.start();

        if(nfcSensor.isNfcSupported() && nfcSensor.isNfcEnabled())
            nfcSensor.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        if(magnetSensor != null)
            magnetSensor.stop();

        if(nfcSensor.isNfcSupported() && nfcSensor.isNfcEnabled())
            nfcSensor.onPause(this);
    }


    //Create the dialog box with instructions:
    public void showInstructions(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.Instructions);

        builder.setMessage(R.string.InstructionText);
        // Set up the button
        builder.setNegativeButton(R.string.Ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
        }});
        builder.show();
    }

    /**
     * starts the game.
     * Launch GameActivity
     */
    public void startGame() {
        magnetSensor.stop();
        gameActivityIntent.putExtra("VR_MODE", vrMode);
        startActivity(gameActivityIntent);
        finish();
    }
}
