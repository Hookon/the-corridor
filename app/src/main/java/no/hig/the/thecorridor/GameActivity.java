package no.hig.the.thecorridor;

import android.media.MediaPlayer;
import android.os.Bundle;

import com.google.vrtoolkit.cardboard.CardboardActivity;

/**
 * The activity for the game itself,
 * handles music and creates the view
 */
public class GameActivity extends CardboardActivity {

    GameView gameView;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If MediaPlayer not already created, create new and load sound file
        if(mp == null) {
            mp = MediaPlayer.create(this, R.raw.backgroundmusic);
            mp.setLooping(true);
        }
        // Start playing background music:
        mp.start();

        // Boolean sendt from MenuActivity used to set stereo rendering on or off
        boolean vrMode = getIntent().getExtras().getBoolean("VR_MODE");
        gameView = new GameView(this);
        gameView.setVRModeEnabled(vrMode);

        //Create and set the view:
        setContentView(gameView);
        setCardboardView(gameView);
    }

    @Override
    public void onStart() {
        super.onStart();
        mp.start();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        mp.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        mp.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        mp.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mp.pause();
    }

    /**
     * clean up when destroying
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mp.pause();
        mp.release();
        mp = null;

        System.gc();
    }
}
