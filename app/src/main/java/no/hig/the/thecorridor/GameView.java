package no.hig.the.thecorridor;

import android.content.Context;

import com.google.vrtoolkit.cardboard.CardboardView;

/**
 * The view where the game is rendered
 */
public class GameView extends CardboardView {

    no.hig.the.thecorridor.Renderer renderer;

    public GameView(Context context) {
        super(context);

        renderer = new no.hig.the.thecorridor.Renderer(context);
        setRenderer(renderer);
    }

    public no.hig.the.thecorridor.Renderer getRenderer() {
        return renderer;
    }
}
