package no.hig.the.thecorridor;

import com.google.vrtoolkit.cardboard.EyeTransform;

import java.util.Random;

/**
 * class for a spider, Initializes and keeps track of animation
 */
public class Spider {
    private final int ANIMATION_DELAY = 10;
    private final int NUM_ANIMATION_STAGE = 3;
    private final float SCALE = 0.1f;

    private int frameCounter;
    private int currentFrame;

    private Mesh spider[];

    private Random random;

    public Spider() {
        spider = new Mesh[NUM_ANIMATION_STAGE];
        currentFrame = 0;
        frameCounter = ANIMATION_DELAY;
        random = new Random();
    }

    /**
     * initialize the spider
     * @param index the index for the animation
     * @param objLoader the object loader with all the object information
     * @param textureHandle the handle for the texture
     * @param shaderProgram the shader we are using for the spiders
     */
    public void initSpider(int index, OBJLoader objLoader, int textureHandle, int shaderProgram) {
        //create the Mesh and set the scale:
        spider[index] = new Mesh(objLoader, textureHandle, shaderProgram);
        spider[index].getTransformation().setScale(new float[]{SCALE, SCALE, SCALE});
        //Set the rotation of the first animation mesh:
        if(index == 0) {
            float rotation = random.nextInt((100 - 1) + 1) + 1;
            spider[index].getTransformation().setRotation(new float[]{rotation, 0, 1, 0});
        }
    }

    /**
     * plays the animation
     * @param eyeTransform the transformations to apply to an eye
     * @param view the view matrix
     */
    public void playAnimation(EyeTransform eyeTransform, float[] view) {

        frameCounter--;
        if(frameCounter < 0) {
            frameCounter = ANIMATION_DELAY;
            currentFrame++;
        }
        if(currentFrame >= NUM_ANIMATION_STAGE)
            currentFrame = 0;
        //set the rotation/transformation like the first one we initialized:
        spider[currentFrame].setTransformation(spider[0].getTransformation());
        spider[currentFrame].update(view, eyeTransform);
        spider[currentFrame].draw();
    }

    /**
     * retrieves the transformation for the first spider animation mesh
     */
    public Transformation getTransformation() {
        return spider[0].getTransformation();
    }

    /**
     * @return the mesh for the first spider:
     */
    public Mesh getMesh(){return spider[0];}

    /**
     * Free GLSL memory used by the spider meshes
     */
    public void freeSpider() {
        for(Mesh mesh : spider)
            mesh.freeMemory();
    }
}
