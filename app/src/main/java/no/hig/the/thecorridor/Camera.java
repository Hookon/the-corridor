package no.hig.the.thecorridor;

import android.opengl.Matrix;

import com.google.vrtoolkit.cardboard.HeadTransform;

/**
 * Contains everything about the camera
 */
public class Camera {
    private float position[];                       // Camera position
    private float viewDirection[];                  // Where the camera is pointing
    private float viewMatrix[];                     // View matrix

    private final float UP[] = {0.0f, 1.0f, 0.0f};  // Const up direction in our world
    private final float MOVEMENT_SPEED = 0.05f;        // Regulate movement speed

    private float tmpViewDirection[];

    /**
     * Init data. Set view direction to -z axis
     */
    public Camera() {
        position = new float[4];
        viewDirection = new float[4];
        tmpViewDirection = new float[4];
        viewMatrix = new float[16];

        position[2] -= 2;
    }

    /**
     * Updates the heads view direction
     * @param headTransform gives us the viewDirection
     */
    public void updateHeadTracking(HeadTransform headTransform) {
        headTransform.getForwardVector(tmpViewDirection, 0);
        viewDirection[1] = tmpViewDirection[1];
        viewDirection[1] *= -1;
        viewDirection[2] = tmpViewDirection[2];
        viewDirection[3] = tmpViewDirection[3];
    }

    /**
     * Generate the view matrix
     * @return the view matrix
     */
    public float[] getViewMatrix() {
        Matrix.setLookAtM(viewMatrix, 0,
                position[0], position[1], position[2],
                (position[0] + ((viewDirection[0]))), (position[1] + ((viewDirection[1]))), (position[2] + (viewDirection[2])),
                UP[0], UP[1], UP[2]);

        return viewMatrix;
    }

    /**
     * Move camera forward
     */
    public void forward() {
        for(int i = 0; i<(position.length -1); i++)
            position[i] += viewDirection[i] * MOVEMENT_SPEED;
    }

    /**
     * Move camera in specified direction
     * @param dir the specified direction
     */
    public void moveInDirection(float[] dir) {
        for(int i = 0; i<(position.length -1); i++)
            position[i] += dir[i] * MOVEMENT_SPEED;
    }

    /**
     * @return position of camera
     */
    public float[] getPosition() {
        return position;
    }

    /**
     * @return the view direction and position added
     */
    public float[] getViewDirectionPosition(){
        int max = 3;
        float viewDirectionPosition[] = new float[max];
        for (int i = 0; i < max; i++) {
            viewDirectionPosition[i] = ((viewDirection[i])) + position[i];
        }
        return viewDirectionPosition;
    }

    /**
     * @return only the view direction
     */
    public float[] getViewDirection() {
        return viewDirection;
    }

    /**
     * set the position of the camera
     * @param position the new position
     */
    public void setPosition(float[] position) {
        this.position = position;
    }
}
