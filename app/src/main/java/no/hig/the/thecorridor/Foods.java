package no.hig.the.thecorridor;

import android.content.Context;

import com.google.vrtoolkit.cardboard.EyeTransform;

import java.util.Random;


/**
 * Container class for the spiders, creates and updates them
 */
public class Foods {
    public final int NUM_SPIDERS = 20;

    private final float X_LIMIT = 0.8f;

    private final float MIN_Z = 7f;         // Min value for Z used to reposition the spider
    private final float MAX_Z = 20f;        // Max value for Z used to reposition the spider

    private final String SPIDER_FILE_ONE = "spider1.obj";
    private final String SPIDER_FILE_TWO = "spider2.obj";
    private final String SPIDER_FILE_THREE = "spider3.obj";

    private Texture spiderTexture;
    private Spider spiders[];

    private Random random;

    public Foods(Context context, int shaderProgram) {
        spiderTexture = new Texture(context, R.drawable.spider);
        spiders = new Spider[NUM_SPIDERS];

        random = new Random();

        OBJLoader objParserOne = new OBJLoader();
        objParserOne.parseOBJ(SPIDER_FILE_ONE, context);

        OBJLoader objParserTwo = new OBJLoader();
        objParserTwo.parseOBJ(SPIDER_FILE_TWO, context);

        OBJLoader objParserThree = new OBJLoader();
        objParserThree.parseOBJ(SPIDER_FILE_THREE, context);

        for(int i = 0; i<NUM_SPIDERS; i++) {
            spiders[i] = new Spider();
            spiders[i].initSpider(0, objParserOne ,spiderTexture.getTextureHandler(), shaderProgram);
            spiders[i].initSpider(1, objParserTwo ,spiderTexture.getTextureHandler(), shaderProgram);
            spiders[i].initSpider(2, objParserThree, spiderTexture.getTextureHandler(), shaderProgram);
        }
    }

    /**
     * Repositions the spiders if they are behind the camera
     * @param pos the position of the camera
     */
    public void update(float pos[]) {
        for(int i = 0; i<NUM_SPIDERS; i++) {
            if((spiders[i].getTransformation().getPosition()[2]) > pos[2]) {

                // Generate random number between -X_LIMIT and X_LIMIT
                float x = (random.nextFloat() * (1 + X_LIMIT)) - X_LIMIT;
                float y = (random.nextFloat() * (1 + X_LIMIT)) - X_LIMIT;
                // Generate random number between MIN_Z and MAX_Z
                float z = random.nextFloat() * (MIN_Z - MAX_Z) + MAX_Z;

                spiders[i].getTransformation().setPosition(new float[]{x, y, pos[2] - z});
            }
        }
    }

    /**
     * draw the spiders:
     * @param eyeTransform the transformations to apply to an eye
     * @param view the view matrix
     */
    public void draw(EyeTransform eyeTransform, float[] view) {
        for(int i = 0; i<NUM_SPIDERS; i++) {
            spiders[i].playAnimation(eyeTransform, view);
        }
    }

    /**
     * return spider mesh
     * @param index animation number
     * @return mesh
     */
    public Spider getSpider(int index){
        return spiders[index];
    }

    /**
     * Free GLES memory used by all the food meshes
     */
    public void freeFoods() {
        for(Spider spider : spiders)
            spider.freeSpider();
	}
}
