package no.hig.the.thecorridor;

import android.opengl.Matrix;

/**
 * The transformation class, takes care of positions, scaling and rotating.
 */
public class Transformation {
    private float[] position;               // Position(x, y z)
    private float[] rotation;               // Rotation(angle, x, y, z)
    private float[] scale;                  // Scale(x, y, z)

    float[] rotationMatrix;
    float[] scaleMatrix;
    float[] translateMatrix;
    float[] modelMatrix;

    public Transformation() {
        // Init Variables
        position = new float[3];
        rotation = new float[4];
        scale = new float[3];

        rotationMatrix = new float[16];
        scaleMatrix = new float[16];
        translateMatrix = new float[16];
        modelMatrix = new float[16];

        // Init position, rotation and scale
        for(int i = 0; i<position.length; i++) {
            position[i] = 0.0f;
            rotation[i] = 0.0f;
            scale[i] = 1.0f;
        }
        rotation[3] = 1.0f;
    }

    public float[] getModel() {
        // Reset matrix to identityMatrix
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.setIdentityM(modelMatrix, 0);

        // Calculate Rotation, translate and scale matrix
        Matrix.setRotateM(rotationMatrix, 0, rotation[0], rotation[1], rotation[2], rotation[3]);
        Matrix.translateM(translateMatrix, 0, position[0], position[1], position[2]);
        Matrix.scaleM(scaleMatrix, 0, scale[0], scale[1], scale[2]);

        // Apply scale, rotation and translate to model matrix
        Matrix.multiplyMM(modelMatrix, 0, translateMatrix, 0, rotationMatrix, 0);
        Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

        return modelMatrix;
    }

    // Getters and setters
    public void setPosition(float[] position) { this.position = position; }

    public void setRotation(float[] rotation) {
        this.rotation = rotation;
    }

    public void setScale(float[] scale) {
        this.scale = scale;
    }

    public float[] getPosition() {
        return position;
    }

    public float[] getRotation() { return rotation; }

    public float[] getScale() {
        return scale;
    }
}
