package no.hig.the.thecorridor;

import android.app.ActionBar;
import android.graphics.Point;
import android.os.Bundle;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.Display;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Tien on 07.12.2014.
 */
public class MenuActivityTest extends ActivityInstrumentationTestCase2<MenuActivity> {

    MenuActivity menuActivity;

    public MenuActivityTest() {
        super(MenuActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        menuActivity = getActivity();
    }

    @MediumTest
    public void testButtonsNotNull() {

        final float MIDDLE_X = 2.5f;
        final float Y_OFFSET_INSTRUCTION = 2.1f;
        final float Y_OFFSET_NEW_GAME = 3.5f;
        final float Y_OFFSET_QUIT_GAME = 1.5f;

        Bundle exstra = menuActivity.getIntent().getExtras();

        //Get the screen size:
        Point point = new Point();
        Display display = menuActivity.getWindowManager().getDefaultDisplay();
        display.getSize(point);

        //Create the button and place them:
        Button startGame = new Button(menuActivity);
        startGame.setText(R.string.New_Game);
        startGame.setX((int)(point.x/MIDDLE_X));
        startGame.setY((int)(point.y/Y_OFFSET_NEW_GAME));

        Button instructions = new Button(menuActivity);
        instructions.setText(R.string.Instructions);
        instructions.setX((int)(point.x/MIDDLE_X));
        instructions.setY((int)(point.y/Y_OFFSET_INSTRUCTION));

        Button endGame = new Button(menuActivity);
        endGame.setText(R.string.Quit_Game);
        endGame.setX((int)(point.x/MIDDLE_X));
        endGame.setY((int)(point.y/Y_OFFSET_QUIT_GAME));

        TextView scoreView = null;

        if(exstra != null) {
            scoreView = new TextView(menuActivity);
            assertNotNull(scoreView);
        }

        assertNull(scoreView);
        assertNotNull(startGame);
        assertNotNull(endGame);
        assertNotNull(instructions);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
