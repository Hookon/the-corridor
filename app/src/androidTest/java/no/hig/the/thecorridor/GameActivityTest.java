package no.hig.the.thecorridor;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

/**
 * Created by Tien on 07.12.2014.
 */
public class GameActivityTest extends ActivityInstrumentationTestCase2<GameActivity> {

    GameActivity gameActivity;

    public GameActivityTest() {
        super(GameActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        gameActivity = getActivity();
    }

    @SmallTest
    public void testGameViewNotNull() {
        assertNotNull(gameActivity.gameView);
    }

    @SmallTest
    public void testBackgroundSoundNotNull() {
        assertNotNull(gameActivity.mp);
    }

    @SmallTest
    public void testOBJLoaderParser() {
        OBJLoader objLoader = new OBJLoader();
        objLoader.parseOBJ("corridor.obj", gameActivity);

        assertNotSame(objLoader.getFaces().size(), 0);
        assertNotSame(objLoader.getVertices().size(), 0);
        assertNotSame(objLoader.getNormals().size(), 0);
        assertNotSame(objLoader.texturecoords.size(), 0);
    }

    @SmallTest
    public void testTextureLoader() {
        assertNotSame(gameActivity.gameView.getRenderer().getCorridorGame().getPlayerTexture().getTextureHandler(), 0);
    }

    @SmallTest
    public void testPhysics() {

        boolean col = Physics.checkCollision(gameActivity.gameView.getRenderer().getCorridorGame().getPlayer().getMainMesh(), gameActivity.gameView.getRenderer().getCorridorGame().getPlayer().getMainMesh(), 0);
        boolean colNot = Physics.checkCollision(gameActivity.gameView.getRenderer().getCorridorGame().getPlayer().getMainMesh(), gameActivity.gameView.getRenderer().getCorridorGame().getPlayer().getMainMesh(), 10000);

        boolean dist = Physics.checkDistanceZ(gameActivity.gameView.getRenderer().getCorridorGame().getPlayer().getMainMesh(), gameActivity.gameView.getRenderer().getCorridorGame().getPlayer().getMainMesh());

        assertEquals(false, colNot);
        assertEquals(true, col);
        assertEquals(true, dist);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
